# Portfolio Site v2

This is the second version of my personal website (v1 can be found [here](https://gitlab.com/seblz432/portfolio-site-v1)). I designed and developed everything myself and I wanted to make something that stood out from other portfolio websites. This website uses [Next.js](https://nextjs.org/), a [React](https://reactjs.org/) framework, with [Framer motion](https://www.framer.com/motion/) for animations.

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) here

## Deployment

This website is deployed on the the [Vercel Platform](https://vercel.com/import?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme).

Check out the [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
