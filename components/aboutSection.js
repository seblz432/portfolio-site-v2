import Image from 'next/image'
import styles from '../styles/aboutSection.module.css'

export default function About() {
  return (
    <div className={styles.section}>
      <div className={styles.imageWrapper}>
        <Image
          className={styles.photo}
          src="/photo.jpg"
          alt='Photo of a path along open grass next to the ocean'
          width={2104}
          height={1560}
          quality={100}
        />
        <span className={styles.imageText}>(I’m a casual photographer in my spare time)</span>
      </div>

      <div className={styles.wrapper}>
        <p className={styles.text}>
          Hi! I'm Sebastian,<br/>the one-man show in <u>Victoria, British Columbia</u><br/><u>making cool shit</u> that gets sales, leads, conversions.<br/>I like to be where <u>ideas meet execution</u>,<br/>making sure that what I build brings <u>real-world value</u>.<br/>I work independently and<br/> sometimes in collaboration with a graphic designer.<br/>

          <span className={styles.lastLine}><u>I'm available</u> for both freelance and contract work.</span>
        </p>
        <p className={styles.mobileText}>
          Hi! I'm Sebastian,<br/>the one-man show in Victoria, British Columbia making cool shit that gets sales, leads, conversions. I like to be where ideas meet execution, making sure that what I build brings real-world value. I work independently and sometimes in collaboration with a graphic designer.<br/>

          I'm available for both freelance and contract work.
        </p>
      </div>
    </div>
  )
}
