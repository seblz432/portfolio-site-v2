import cx from 'classnames'
import { motion } from 'framer-motion'
import { useState } from 'react'
import styles from '../styles/menu.module.css'

export default function Projects() {
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(!open);

    if (open) {
      document.documentElement.style.setProperty('--scroll', 'auto');
    } else {
      document.documentElement.style.setProperty('--scroll', 'hidden');
    }
  }

  return (
    <>
      <motion.a
        href=""
        initial={{ opacity: 0}}
        animate={{ opacity: 1}}
        transition={{ type: "spring", delay: 1, duration: 1.1 }}
      >
        <span className={styles.text}>About me</span>
      </motion.a>
      {/*<motion.svg
        className={ cx( styles.ham, styles.hamRotate, styles.ham8, open ? styles.active : '') }
        viewBox="0 0 100 100"
        width="80"
        onClick={handleClick}

        initial={{ opacity: 0}}
        animate={{ opacity: 1}}
        transition={{ type: "spring", delay: 0.75, duration: 1 }}
      >
        <path
          className={ cx(styles.line, styles.top) }
          d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20" />
        <path
          className={ cx(styles.line, styles.middle) }
          d="m 30,50 h 40" />
        <path
          className={ cx(styles.line, styles.bottom) }
          d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20" />
      </motion.svg>

      <motion.div
        className={styles.overlay}
        initial={{ opacity: 0}}
        animate={{
          opacity: open ? 1 : 0,
          visibility: open ? 'visible' : 'hidden',
        }}
      />*/}
    </>
  )
}
