import Image from 'next/image'
import styles from '../styles/projectsSection.module.css'

export default function Projects() {
  return (
    <div className={styles.section}>
      <div className={styles.wrapper}>
        <div className={styles.titleBackground}>
          <h1 className={styles.title}>Featured Work</h1>
        </div>

        <div className={styles.project} style={{ alignSelf: 'flex-start', textAlign: 'left' }}>
          <a className={styles.projectImageContainer} href="https://garrettshannon.com" target="_blank" rel="noopener noreferrer">
            <Image
              className={styles.projectImage}
              src='/garrettshannon.com.png'
              alt='Screenshot of garrettshannon.com'
              width={1908}
              height={979}
              loading='eager'
            />
          </a>
          <h2 className={styles.about}>Design & Development</h2>
          <h2 className={styles.client}>Garrett Shannon</h2>
        </div>

        <div className={styles.project} style={{ alignSelf: 'flex-end', textAlign: 'right' }}>
          <a className={styles.projectImageContainer} href="https://sepia.co" target="_blank" rel="noopener noreferrer">
            <Image
              className={styles.projectImage}
              src='/sepia.co.jpg'
              alt='Screenshot of sepia.co'
              width={1908}
              height={979}
              loading='eager'
            />
          </a>
          <h2 className={styles.about}>Design & Development</h2>
          <h2 className={styles.client}>Sepia.co</h2>
        </div>

        <div className={styles.project} style={{ alignSelf: 'flex-start', textAlign: 'left' }}>
          <a className={styles.projectImageContainer} href="https://featured.jlhcapitalpartners.com" target="_blank" rel="noopener noreferrer">
            <Image
              className={styles.projectImage}
              src='/featured.jlhcapitalpartners.com.png'
              alt='Screenshot of featured.jlhcapitalpartners.com'
              width={1908}
              height={979}
              loading='eager'
            />
          </a>
          <h2 className={styles.about}>Development</h2>
          <h2 className={styles.client}>JLH Capital Partners</h2>
        </div>

        <div className={styles.project} style={{ alignSelf: 'flex-end', textAlign: 'right' }}>
          <a className={styles.projectImageContainer} href="https://beecofriendly.ca" target="_blank" rel="noopener noreferrer">
            <Image
              className={styles.projectImage}
              src='/beecofriendly.ca.jpg'
              alt='Screenshot of beecofriendly.ca'
              width={1908}
              height={979}
              loading='eager'
            />
          </a>
          <h2 className={styles.about}>Design & Development</h2>
          <h2 className={styles.client}>Be Eco-Friendly</h2>
        </div>

        <div className={styles.project} style={{ alignSelf: 'flex-start', textAlign: 'left' }}>
          <a className={styles.projectImageContainer} href="https://voyageenquintessence.be" target="_blank" rel="noopener noreferrer">
            <Image
              className={styles.projectImage}
              src='/voyageenquintessence.be.jpg'
              alt='Screenshot of voyageenquintessence.be'
              width={1908}
              height={979}
              loading='eager'
            />
          </a>
          <h2 className={styles.about}>Design & Development</h2>
          <h2 className={styles.client}>Voyage en Quintessence</h2>
        </div>
      </div>
    </div>
  )
}
