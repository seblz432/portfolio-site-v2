import Image from 'next/image'
import { motion } from 'framer-motion'
import { useEffect, useState } from 'react'
import styles from '../styles/homeSection.module.css'

export default function Home() {

  /* Scroll progress is the percentage of the viewport height scrolled */
  const [scrollProgress, setSrollProgress] = useState(0);
  const handleScroll = () => {
      let position = window.pageYOffset;
      if (window.innerWidth < 1000 && window.innerHeight < 630) {
        position = 0;
      }
      setSrollProgress(position / window.innerHeight);
  };

  useEffect(() => {
      window.addEventListener('scroll', handleScroll, { passive: true });

      return () => {
          window.removeEventListener('scroll', handleScroll);
      };
  }, []);

  return (
    <motion.div
      initial={{ scale: 1, width: '0px', marginLeft: '50%' }}
      animate={{ scale: 1, width: '100vw', marginLeft: '0%' }}
      transition={{ type: "spring", duration: 1.1 }}
    >
      <div className={styles.section}>
      <motion.div
        animate={{ opacity: (1 - scrollProgress) }}
        transition={{ duration: 0 }}
      >
      <motion.div
        className={styles.container}
        initial={{ opacity: 0, scale: 1.1 }}
        animate={{ opacity: 1, scale: 1 }}
        transition={{ type: "spring", delay: 0.38, duration: 1}}
      >

        <div className={styles.topHalf}>
          {/*<HelloThere className={styles.hello}/>*/}
            <h1 className={styles.name}>Sebastian<br/>Gale</h1>

            <div className={styles.me}>
              <div className={styles.meContainer}>
                <div className={styles.rectangle}/>
                  <img
                    className={styles.portrait}
                    src='/portrait.png'
                    alt="Portrait of Sebastian Gale"
                  />
                <h2 className={styles.creative}>Creative web developer</h2>
              </div>
            </div>
        </div>

        <div className={styles.bottomHalf}>
          <h2><span className={styles.bottomText1}>I create intuitive experiences on the web that</span> <span className={styles.bottomText2}>stand out</span></h2>
        </div>

      </motion.div>
      </motion.div>
      </div>
    </motion.div>
  )
}
