import { useState } from 'react';
import { motion } from "framer-motion"
import styles from '../styles/contact.module.css'

export default function Home() {
  const [open, setOpen] = useState(false);
  const [hover, setHover] = useState(false);

  const handleOpen = (event, info) => {
    setOpen(true);
    setHover(false);
    document.documentElement.style.setProperty('--blur', '3px');
    document.documentElement.style.setProperty('--scroll', 'hidden');
  }

  const handleClose = (event, info) => {
    setOpen(false);
    document.documentElement.style.setProperty('--blur', '0');

    setTimeout(() => {
      document.documentElement.style.setProperty('--scroll', 'auto');
    }, 200);
  }

  const onHover = (event, info) => {
    setHover(true);
  }
  const onHoverEnd = (event, info) => {
    setHover(false);
  }

  return (
    <div id="contact">
      {/* DESKTOP */}
      <motion.button
        className={styles.container}
        initial={{ marginLeft: '-35px', borderRadius: '2px' }}
        whileHover={onHover}
        onHoverEnd={onHoverEnd}
        animate={{
          marginLeft: '0px',
          width: open ? '500px' : ( hover ? '50px' : '' ),
          borderRadius: open ? '5px' : ( hover ? '5px' : '2px' ),
          cursor: open ? 'default' : '',
        }}
        onTap={handleOpen}
      >
        <motion.div
          className={styles.wrapper}
          animate={{
            width: open ? '500px' : ''
          }}
          transition={{ duration: open ? 0 : 0 }}
        >
          <motion.span animate={{ paddingBottom: open ? '35px' : '0px' }} className={styles.title}>CONTACT</motion.span>

          <motion.span
            className={styles.contactMethod}
            inital={{ opacity: 0 }}
            animate={{ display: open ? 'block' : '', opacity: 1 }}
            transition={{ type: 'spring', delay: open ? 0 : 0 }}
          >
            <span className={styles.label}>Email: </span><a className={styles.link} href="mailto:contact@sebastiangale.ca">contact@sebastiangale.ca</a>
          </motion.span>

          <motion.span
            className={styles.contactMethod}
            inital={{ opacity: 0 }}
            animate={{ display: open ? 'block' : '', opacity: 1 }}
            transition={{ type: 'spring', delay: open ? 0 : 0 }}
          >
            <span className={styles.label}>Phone: </span><a className={styles.link} href="tel:+17786500257">+1 (778) 650-0257</a>
          </motion.span>
        </motion.div>
      </motion.button>

      {/* MOBILE */}
      <motion.div
        style={{  }}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ type: "spring", delay: 1.5, duration: 1}}
      >
        <motion.button
          className={styles.containerMobile}
          animate={{
            top: open ? 'calc(50% - 100px)' : 'auto',
          }}
          transition={{ type: 'spring', bounce: 0.15, duration: 0.5 }}
          onTap={handleOpen}
        >
          <motion.div
            className={styles.wrapperMobile}
            animate={{
              paddingTop: open ? '30px' : '2px',
            }}
            transition={{ type: 'spring', bounce: 0.25, duration: 0.5 }}
          >
            <span className={styles.title}>CONTACT</span>

            <span className={styles.contactMethod}><span className={styles.label}>Email: </span><a className={styles.link} href="mailto:dev@sebastiangale.ca">dev@sebastiangale.ca</a></span>
            <span className={styles.contactMethod}><span className={styles.label}>Phone: </span><a className={styles.link} href="tel:+17786500257">+1 (778) 650-0257</a></span>
          </motion.div>
        </motion.button>
      </motion.div>

      <motion.div
        className={styles.overlay}
        animate={{ display: (open) ? 'block' : '' }}
        onTap={handleClose}
      />
    </div>
  )
}
