
import Head from 'next/head'
import styles from '../styles/Home.module.css'

import HomeSection from '../components/homeSection'
import AboutSection from '../components/aboutSection'
import ProjectsSection from '../components/projectsSection'
import Contact from '../components/contact'

import Menu from '../components/menu'

export default function Home() {
  return (
    <div className={styles.container} lang="en">
      <Head>
        <title>Sebastian Gale | Creative Web Developer</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="description" key="description" content="Hi! I'm Sebastian, the one-man show in Victoria, British Columbia making cool shit that gets sales, leads, conversions. I like to be where ideas meet execution, making sure that what I build brings real-world value. I work independently and sometimes in collaboration with a graphic designer. I'm available for both freelance and contract work."/>
      </Head>

      <Contact/>

      {/*<Menu/>*/}

      <main className={styles.main}>
        <HomeSection/>
        <AboutSection/>
        <ProjectsSection/>
      </main>
    </div>
  )
}
